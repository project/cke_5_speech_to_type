/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md.
 */

import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import { createRecognition, isSupported } from './speechAPI';

export default class speechtotype extends Plugin {

	 static get pluginName() {
		 return 'SpeechToType';
	 }

	 init() {
		 const editor = this.editor;

		 editor.ui.componentFactory.add( 'speechtotype', () => {
			 // The button will be an instance of ButtonView.
			 const button = new ButtonView();

			 button.set( {
				 tooltip: 'microphone',
				 icon: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-mic-fill" viewBox="0 0 16 16"><path d="M5 3a3 3 0 0 1 6 0v5a3 3 0 0 1-6 0V3z"/><path d="M3.5 6.5A.5.5 0 0 1 4 7v1a4 4 0 0 0 8 0V7a.5.5 0 0 1 1 0v1a5 5 0 0 1-4.5 4.975V15h3a.5.5 0 0 1 0 1h-7a.5.5 0 0 1 0-1h3v-2.025A5 5 0 0 1 3 8V7a.5.5 0 0 1 .5-.5z"/></svg>'
			 } );

			 //Execute a callback function when the button is clicked
			 button.on( 'execute', () => {
				 const cmd = this;
				 let recognition = cmd.recognition;

				 if ( !isSupported ) {
					 return;
				 }
				 if ( !recognition ) {
					 cmd.recognition = recognition = createRecognition( editor );
				 }
				 if (!button.isOn) {
					 button.isOn=true;
					 recognition.start();
				 } else if (button.isOn) {
					 button.isOn=false;
					 recognition.stop();
				 }
			 } );

			 return button;
		 } );
	 }
 }