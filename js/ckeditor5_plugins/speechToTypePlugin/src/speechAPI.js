const SpeechAPI = window.speechRecognition || window.webkitSpeechRecognition;
const isSupported = !!SpeechAPI;

function createRecognition( editor ) {
	const recognition = new SpeechAPI();

	recognition.continuous = true;
	recognition.maxAlternatives = 1;
	recognition.lang = editor.config.contentsLanguage || editor.langCode;

	recognition.addEventListener( 'result', ( evt ) => {

				//Change the model using the model writer
				editor.model.change( writer => {
		for ( let i = evt.resultIndex; i < evt.results.length; i++ ) {
            editor.model.insertContent( writer.createText( `${ event.results[ i ][ 0 ].transcript } ` ) );
			// editor.insertHtml( `${ event.results[ i ][ 0 ].transcript } ` );
		}
    } );
	}, false );

	return recognition;
}

export { isSupported };
export { createRecognition };